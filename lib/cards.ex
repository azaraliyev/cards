defmodule Cards do
  @moduledoc """
    Provides methods for creating and handling a deck of cards.
  """

  @doc """
    Creates a deck of cards.
  """
  def create_deck do
    values = ["Ace", "Two", "Three", "Four", "Five"]
    suits = ["Spades", "Clubs", "Hearts", "Diamonds"]

    for suit <- suits, value <- values do
      "#{value} of #{suit}"
    end
  end

  @doc """
    Shuffles a deck of card.
  """
  def shuffle(deck) do
    Enum.shuffle(deck)
  end

  @doc """
    Checks whether the deck contains a given card.

  ## Examples

      iex> deck = Cards.create_deck
      iex> Cards.contains?(deck, "Ace of Spades")
      true
      
  """
  def contains?(deck, card) do
    Enum.member?(deck, card)
  end

  @doc """
    Divides a deck into a hand and the reminder of the deck.
    The `hand_size` argument indicates how many cards should
    be in the hand.

  ## Examples

      iex> deck = Cards.create_deck
      iex> {hand, _deck} = Cards.deal(deck, 1)
      iex> hand
      ["Ace of Spades"]

  """
  def deal(deck, hand_size) do
    Enum.split(deck, hand_size)
  end

  @doc """
    Saves provided deck to a file in the data folder.
    The `filename` argument indicates a file name without extension.

  ## Examples

    iex> deck = Cards.create_deck
    iex> Cards.save(deck, "deck")
    :ok

  """
  def save(deck, filename) do
    binary = :erlang.term_to_binary(deck)
    File.write("data/#{filename}.crd", binary)
  end

  @doc """
    Loads a given file from the data folder.
  """
  def load(filename) do
    case File.read("data/#{filename}.crd") do
      {:ok, binary} -> :erlang.binary_to_term(binary)
      {:error, _reason} -> "File not found!"
    end
  end

  @doc """
    Creates a hand by piping create, shuffle, and then deal methods.
  """
  def create_hand(hand_size) do
    Cards.create_deck
    |> Cards.shuffle
    |> Cards.deal(hand_size)
  end
end
